/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */

export type CharList = Array<string | number>;

export const ALPHA: CharList = [];

for (let i = 'A'.charCodeAt(0); i <= 'Z'.charCodeAt(0); i++) {
  // Upper case
  ALPHA.push(String.fromCharCode(i));

  // Lower case
  ALPHA.push(String.fromCharCode(i + 0x20));
}

export const NUM: CharList = [
  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
];

export const ALPHANUM: CharList = ALPHA.concat(NUM);

export const TAG: CharList = ALPHANUM.concat(['_', '.']);

export const LINEWS: CharList = [' ', '\t'];

export const LINEEND: CharList = ['\r', '\n']

export const ALLWS: CharList = LINEWS.concat(LINEEND);

export const SEP: CharList = [':', '='];

export const ENDKEY: CharList = LINEWS.concat(SEP);
