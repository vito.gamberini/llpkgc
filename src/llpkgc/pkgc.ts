/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */

import { LLParse, source } from "llparse";
import { TAG, LINEEND, LINEWS, ALLWS, ENDKEY } from "./constants";

const p = new LLParse('llpkgc__internal');

p.property('ptr', 'settings');
p.property('i8', 'unfinished_');
p.property('i8', 'escaped_');

const comment = p.node('comment');

const line_start = p.node('line_start');

const on_line_begin = p.code.match('llpkgc__line_begin');

const expect_sep = p.node('expect_sep');

const expect_value = p.node('expect_value');
const maybe_LF = p.node('maybe_LF');
const maybe_CR = p.node('maybe_CR');

const key_span = p.span(p.code.span('llpkgc__key_span'));
const key = p.node('key');

const on_keyword = p.code.match('llpkgc__keyword_complete');
const on_variable = p.code.match('llpkgc__variable_complete');

const maybe_variable = p.node('maybe_variable');

const value_complete = p.code.match('llpkgc__value_complete');

const literal_complete = p.code.match('llpkgc__vallit_complete');
const literal_span = p.span(p.code.span('llpkgc__vallit_span'));
const literal = p.node('literal');

const maybe_escaped = p.node('maybe_escaped');
const is_escaped = p.code.update('escaped_', 1);

const variable_complete = p.code.match('llpkgc__valvar_complete');
const variable_span = p.span(p.code.span('llpkgc__valvar_span'));
const variable = p.node('variable');

const variable_close = p.node('variable_close');

const skip_one = (name: string, next: source.node.Node) =>
  p.node(name).skipTo(next);

comment
  .match(LINEEND, line_start)
  .skipTo(comment)

const invoke_line_begin = p.invoke(on_line_begin, {
  0: key_span.start(key)
}, p.error(99, 'Line start error'))

line_start
  .match(ALLWS, line_start)
  .match('#', comment)
  .peek(TAG, invoke_line_begin)
  .otherwise(p.error(2, 'Expected key'));

key
  .match(TAG, key)
  .peek(ENDKEY, key_span.end(expect_sep))
  .otherwise(p.error(4, 'Invalid key character'));


const keyword_invoke = p.invoke(on_keyword, {
  0: expect_value
}, p.error(5, 'Keyword complete error'));

const variable_invoke = p.invoke(on_variable, {
  0: expect_value
}, p.error(6, 'Variable complete error'));

expect_sep
  .match(LINEWS, expect_sep)
  .match(':', keyword_invoke)
  .match('=', variable_invoke)
  .otherwise(p.error(7, 'Expected seperator'));

const invoke_value = p.invoke(value_complete, {
  0: line_start
}, p.error(11, 'Value complete error'));

expect_value
  .match(LINEWS, expect_value)
  .peek(['\r', '\n', '#'], invoke_value)
  .match('$', maybe_variable)
  .otherwise(literal_span.start(literal));

maybe_LF
  .match('\n', expect_value)
  .otherwise(expect_value)

maybe_CR
  .match('\r', expect_value)
  .otherwise(expect_value)

maybe_variable
  .peek('$', literal_span.start(skip_one('variable_skip_dollar', literal)))
  .match('{', variable_span.start(variable))
  .otherwise(p.error(9, 'Unexpected `$`'));

const invoke_literal = (next: source.node.Node) => p.invoke(literal_complete, {
  0: next
}, p.error(10, 'Literal complete error'));

const invoke_lit_value = (next: source.node.Node) => invoke_literal(
  p.invoke(value_complete, {
    0: next
  }, p.error(11, 'Value complete error'))
);

literal
  .peek('$', literal_span.end(invoke_literal(
    skip_one('literal_skip_dollar', maybe_variable)
  )))
  .peek('#', literal_span.end(invoke_lit_value(comment)))
  .peek(LINEEND, literal_span.end(invoke_lit_value(line_start)))
  .match('\\', maybe_escaped)
  .skipTo(literal);

maybe_escaped
  .peek('\r', p.invoke(is_escaped).otherwise(literal_span.end(
    invoke_literal(skip_one('skip_escaped_CR', maybe_LF))
  )))
  .peek('\n', p.invoke(is_escaped).otherwise(literal_span.end(
    invoke_literal(skip_one('skip_escaped_LF', maybe_CR))
  )))
  .peek('#', p.invoke(is_escaped).otherwise(literal_span.end(
    invoke_literal(literal_span.start(skip_one('literal_skip_hash', literal)))
  )))
  .otherwise(literal)

const invoke_variable = p.invoke(variable_complete, {
  0: skip_one('variable_skip_curly', variable_close)
}, p.error(12, 'Variable complete error'));

variable
  .match(TAG, variable)
  .peek('}', variable_span.end(invoke_variable))
  .otherwise(p.error(13, 'Invalid variable character'));

variable_close
  .peek(['\r', '\n', '#'], invoke_value)
  .match('$', maybe_variable)
  .otherwise(literal_span.start(literal));

export const build_llpkgc = () => p.build(line_start);
