# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details

cmake_minimum_required(VERSION 3.13...3.29 FATAL_ERROR)

project(llpkgc)

add_library(llpkgc)
target_sources(llpkgc PRIVATE llpkgc__internal.c llpkgc.c)
target_sources(llpkgc PUBLIC
  FILE_SET HEADERS
  FILES llpkgc__internal.h llpkgc.h
)
