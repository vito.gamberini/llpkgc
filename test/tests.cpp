#include <filesystem>
#include <fstream>
#include <iostream>
#include <string_view>
#include <vector>

#include <catch2/catch_test_macros.hpp>
#include <llpkgc.h>

#include "PkgCParser.hpp"

static std::vector<char> read_file(std::filesystem::path path) {
  auto s {std::filesystem::file_size(path)};
  std::vector<char> vec(s);
  std::ifstream {path, std::ios::binary}.read(vec.data(), s);
  return vec;
}

struct BufParse {
  std::vector<char> buf;
  PkgCParser p;
};

static BufParse parse_and_finish(std::filesystem::path path,
    llpkgc_errno_t finish_status = PCE_OK) {
  BufParse bp {read_file(path)};
  REQUIRE(bp.p.parse(bp.buf.data(), bp.buf.size()) == PCE_OK);
  REQUIRE(bp.p.finish() == finish_status);
  return bp;
}


typedef std::vector<PCEntry> ResultSequence;

static bool verify_result(const PkgCParser& p, const ResultSequence& rs) {
  const auto& data {p.data()};
  if(data.size() != rs.size())
    return false;

  auto i {data.begin()};
  auto j {rs.begin()};

  for(; i != data.end(); ++i, ++j) {
    if(i->is_variable != j->is_variable || i->key != j->key ||
        i->val.size() != j->val.size())
      return false;

    auto k {i->val.begin()};
    auto l {j->val.begin()};

    for(; k != i->val.end(); ++k, ++l) {
      if(k->is_variable != l->is_variable || k->sv != l->sv)
        return false;
    }
  }

  return true;
}

TEST_CASE("comment.pc") {
  auto bp {parse_and_finish("pkgconfs/comment.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("empty-key-no-NL.pc") {
  auto bp {parse_and_finish("pkgconfs/empty-key-no-NL.pc")};
  ResultSequence rs {
      {false, "key1", {}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("empty-key.pc") {
  auto bp {parse_and_finish("pkgconfs/empty-key.pc")};
  ResultSequence rs {
      {false, "key1", {}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("empty.pc") {
  parse_and_finish("pkgconfs/empty.pc");
}

TEST_CASE("escaped-comment.pc") {
  auto bp {parse_and_finish("pkgconfs/escaped-comment.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}, {false, "#Comment"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("escaped-line-no-NL.pc") {
  auto bp {parse_and_finish("pkgconfs/escaped-line-no-NL.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}, {false, "key2: bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("escaped-line.pc") {
  auto bp {parse_and_finish("pkgconfs/escaped-line.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}, {false, "key2: bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("escaped-var.pc") {
  auto bp {parse_and_finish("pkgconfs/escaped-var.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "${foo}"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("keyword.pc") {
  auto bp {parse_and_finish("pkgconfs/keyword.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("literal-no-NL.pc") {
  auto bp {parse_and_finish("pkgconfs/literal-no-NL.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("unescaped-backslash-EOF.pc") {
  auto bp {parse_and_finish("pkgconfs/unescaped-backslash-EOF.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo\\"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("unescaped-backslash.pc") {
  auto bp {parse_and_finish("pkgconfs/unescaped-backslash.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "f\\oo"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("var-value-no-NL.pc") {
  auto bp {parse_and_finish("pkgconfs/var-value-no-NL.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}}},
      {false, "key2", {{true, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("var-value.pc") {
  auto bp {parse_and_finish("pkgconfs/var-value.pc")};
  ResultSequence rs {
      {false, "key1", {{true, "foo"}}},
      {false, "key2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("var.pc") {
  auto bp {parse_and_finish("pkgconfs/var.pc")};
  ResultSequence rs {
      {true, "var1", {{false, "foo"}}},
      {true, "var2", {{false, "bar"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}

TEST_CASE("whitespace.pc") {
  auto bp {parse_and_finish("pkgconfs/whitespace.pc")};
  ResultSequence rs {
      {false, "key1", {{false, "foo"}}},
      {false, "key2", {{false, "bar   \t\t"}}},
  };
  REQUIRE(verify_result(bp.p, rs));
}
