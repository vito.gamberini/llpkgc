#ifndef LLPKGC_PKGCPARSER_HPP
#define LLPKGC_PKGCPARSER_HPP

#include <cstddef>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

#include <llpkgc.h>

struct ValueElement {
  bool is_variable;
  std::string_view sv;
};

struct PCEntry {
  bool is_variable;
  std::string_view key;
  std::vector<ValueElement> val;
};

class PkgCParser : llpkgc_t {
public:
  PkgCParser() {
    llpkgc_init(static_cast<llpkgc_t*>(this), &settings_);
  };

  llpkgc_errno_t parse(char* data, size_t len) {
    auto ret {llpkgc_execute(static_cast<llpkgc_t*>(this), data, len)};
    if(ret != PCE_OK && ret != PCE_PAUSED)
      throw std::runtime_error {"pkgc error"};
    return ret;
  }

  llpkgc_errno_t finish() {
    return llpkgc_finish(static_cast<llpkgc_t*>(this));
  }

  std::vector<PCEntry>& data() {
    return data_;
  }

  const std::vector<PCEntry>& data() const {
    return data_;
  }

private:
  int on_span_next(const char*, std::size_t len) {
    len_ += len;
    return 0;
  }

  static int on_span_next_tr(llpkgc_t* parser, const char* at,
      std::size_t len) {
    return static_cast<PkgCParser*>(parser)->on_span_next(at, len);
  }

  int on_key(const char* at, std::size_t len) {
    p_ = at;
    len_ = len;
    settings_.on_key = on_span_next_tr;
    return 0;
  }

  static int on_key_tr(llpkgc_t* parser, const char* at, std::size_t len) {
    return static_cast<PkgCParser*>(parser)->on_key(at, len);
  }

  int on_keyword_complete() {
    data_.emplace_back(false, std::string_view {p_, len_});
    settings_.on_key = on_key_tr;
    return 0;
  }

  static int on_keyword_complete_tr(llpkgc_t* parser) {
    return static_cast<PkgCParser*>(parser)->on_keyword_complete();
  }

  int on_variable_complete() {
    data_.emplace_back(true, std::string_view {p_, len_});
    settings_.on_key = on_key_tr;
    return 0;
  }

  static int on_variable_complete_tr(llpkgc_t* parser) {
    return static_cast<PkgCParser*>(parser)->on_variable_complete();
  }

  int on_value_literal(const char* at, std::size_t len) {
    p_ = at;
    len_ = len;
    settings_.on_value_literal = on_span_next_tr;
    return 0;
  }

  static int on_value_literal_tr(llpkgc_t* parser, const char* at,
      std::size_t len) {
    return static_cast<PkgCParser*>(parser)->on_value_literal(at, len);
  }

  int on_value_literal_complete() {
    settings_.on_value_literal = on_value_literal_tr;
    if(len_)
      data_.back().val.emplace_back(false, std::string_view {p_, len_});
    return 0;
  }

  static int on_value_literal_complete_tr(llpkgc_t* parser) {
    return static_cast<PkgCParser*>(parser)->on_value_literal_complete();
  }

  int on_value_variable(const char* at, std::size_t len) {
    p_ = at;
    len_ = len;
    settings_.on_value_variable = on_span_next_tr;
    return 0;
  }

  static int on_value_variable_tr(llpkgc_t* parser, const char* at,
      std::size_t len) {
    return static_cast<PkgCParser*>(parser)->on_value_variable(at, len);
  }

  int on_value_variable_complete() {
    settings_.on_value_variable = on_value_variable_tr;
    data_.back().val.emplace_back(true, std::string_view {p_, len_});
    return 0;
  }

  static int on_value_variable_complete_tr(llpkgc_t* parser) {
    return static_cast<PkgCParser*>(parser)->on_value_variable_complete();
  }

  llpkgc_settings_t settings_ {
      .on_key = on_key_tr,
      .on_value_literal = on_value_literal_tr,
      .on_value_variable = on_value_variable_tr,
      .on_keyword_complete = on_keyword_complete_tr,
      .on_variable_complete = on_variable_complete_tr,
      .on_value_literal_complete = on_value_literal_complete_tr,
      .on_value_variable_complete = on_value_variable_complete_tr,
  };

  const char* p_;
  std::size_t len_;
  std::vector<PCEntry> data_;
};

#endif // LLPKGC_PKGCPARSER_HPP
