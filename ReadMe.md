# llpkgc

This is an LL(1) pkg-config file format parser designed in the same vein as
[llhttp](https://github.com/nodejs/llhttp). This code base is _substantially_
derived from llhttp and seeks to emulate its API.

## How to generate the parser

* Clone and navigate to this repo

* `npm install && npm run build`

* The parser source code and CML are located in the `build/llpkgc` folder

## API

### llpkgc_settings_t

The settings object contains a list of callbacks that the parser will invoke.

The following callbacks can return `0` (proceed normally), `-1` (error) or
`PCE_PAUSED` (pause the parser):

* `on_line_begin`: Invoked when a new logical line of the pc file starts.
* `on_keyword_complete`: Invoked when the `:` delimiter is encountered for a
  keyword.
* `on_variable_complete`: Invoked when the `=` delimiter is encountered for a
  variable.
* `on_value_literal_complete`: Invoked after a literal segment of a value has
  been parsed. This may be called after only a zero-length `on_value_literal`
  if the only member of the literal is an escape character.
* `on_value_variable_complete`: Invoked after a variable segment of a value has
  been parsed.
* `on_value_complete`: Invoked after all literal and variable segments of a
  value have been parsed. This is equivalent to the end of the current line.
* `on_pkgc_complete`: Invoked when a pc file has been completely parsed.

The following callbacks can return `0` (proceed normally), `-1` (error) or
`PCE_USER` (error from the callback):

* `on_key`: Invoked when another character of the key is received (variable or
  keyword).
* `on_value_literal`: Invoked when another character of a literal segment is
  recieved. This may be called with a length of zero if the only member of the
  literal is an escape character.
* `on_value_variable`: Invoked when another character of a variable segment is
  recieved.

### `void llpkgc_init(llpkgc_t* parser, const llpkgc_settings_t* settings)`

Initialize the parser with user settings.

### `void llpkgc_reset(llpkgc_t* parser)`

Reset an already initialized parser back to the start state, preserving the
existing callback settings and user data.

### `void llpkgc_settings_init(llpkgc_settings_t* settings)`

Initialize the settings object.

### `llpkgc_errno_t llpkgc_execute(llpkgc_t* parser, const char* data, size_t len)`

Parse full or partial pc data, invoking user callbacks along the way.

If any of `llpkgc_data_cb` returns errno not equal to `PCE_OK` - the parsing
interrupts, and such errno is returned from `llpkgc_execute()`. If `PCE_PAUSED`
was used as an errno, the execution can be resumed with `llpkgc_resume()` call.

**If this function ever returns a non-pause type error, it will continue to
return the same error upon each successive call up until `llpkgc_init()` is
called.**

### `llpkgc_errno_t llpkgc_finish(llpkgc_t* parser)`

This method should be called when the input has reached EOF

This method will invoke `on_pkgc_complete()` callback if the file was terminated
safely. Otherwise an error code will be returned.

### `void llpkgc_pause(llpkgc_t* parser)`

Make further calls of `llpkgc_execute()` return `CPE_PAUSED` and set appropriate
error reason.

**Do not call this from user callbacks! User callbacks must return
`CPE_PAUSED` if pausing is required.**

### `void llpkgc_resume(llpkgc_t* parser)`

Might be called to resume the execution after the pause in user's callback.

See `llpkgc_execute()` above for details.

**Call this only if `llpkgc_execute()` returns `CPE_PAUSED`.**

### `llpkgc_errno_t llpkgc_get_errno(const llpkgc_t* parser)`

Returns the latest error.

### `const char* llpkgc_get_error_reason(const llpkgc_t* parser)`

Returns the verbal explanation of the latest returned error.

**User callback should set error reason when returning the error. See
`llpkgc_set_error_reason()` for details.**

### `void llpkgc_set_error_reason(llpkgc_t* parser, const char* reason)`

Assign verbal description to the returned error. Must be called in user
callbacks right before returning the errno.

**`CPE_USER` error code might be useful in user callbacks.**

### `const char* llpkgc_get_error_pos(const llpkgc_t* parser)`

Returns the pointer to the last parsed byte before the returned error. The
pointer is relative to the `data` argument of `llpkgc_execute()`.

**This method might be useful for counting the number of parsed bytes.**

### `const char* llpkgc_errno_name(llpkgc_errno_t err)`

Returns textual name of error code.
